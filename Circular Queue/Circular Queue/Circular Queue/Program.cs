﻿using System;

namespace Circular_Queue
{
    internal class Program
    {
        static void Main(string[] args)
        {
            IQueue<int> queue = new CircularQueue<int>();

            while (true)
            {
                try
                {
                    if (int.TryParse(Console.ReadLine(), out int result))
                    {
                        queue.Enqueue(result);
                    }
                    else
                    {
                        Console.WriteLine("Dequeued: " + queue.Dequeue());
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }
    }
}
