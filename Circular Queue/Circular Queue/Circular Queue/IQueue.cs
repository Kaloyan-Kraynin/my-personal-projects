﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Circular_Queue
{
    public interface IQueue<T>
    {
        int Size { get; }

        bool IsEmpty { get; }

        void Enqueue(T element);

        T Dequeue();

        T Peek();
    }
}
