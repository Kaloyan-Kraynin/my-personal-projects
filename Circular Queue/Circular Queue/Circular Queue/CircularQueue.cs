﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Circular_Queue
{
    public class CircularQueue<T> : IQueue<T>
    {
        //Code for testing this queue implementation is in Program.cs

        private T[] items;
        private int tail;
        private int head;
        private int size;

        public CircularQueue(int capacity = 4)
        {
            this.items = new T[capacity];
            this.tail = -1;
            this.head = 0;
            this.size = 0;
        }

        public int Size
        {
            get
            {
                return size;
            }
        }

        public bool IsEmpty
        {
            get
            {
                return size == 0;
            }
        }

        public void Enqueue(T element)
        {
            this.tail = ++this.tail % this.items.Length;
            if (size >= 1 && this.tail == this.head)
            {
                this.Resize();
            }
            this.items[this.tail] = element;
            size++;
        }


        public T Dequeue()
        {
            T removed = this.Peek();
            this.head = ++this.head % this.items.Length;
            size--;
            return removed;
        }

        public T Peek()
        {
            if (this.IsEmpty)
            {
                throw new InvalidOperationException("Queue is empty.");
            }
            return this.items[this.head];
        }

        private void Resize()
        {
            int length = this.items.Length;
            T[] temp = new T[length * 2];
            int newPosition = 0;

            for (int oldPosition = this.head; oldPosition < length; oldPosition++)
            {
                temp[newPosition] = this.items[oldPosition];
                newPosition++;
            }
            for (int oldPosition = 0; oldPosition < this.head; oldPosition++)
            {
                temp[newPosition] = this.items[oldPosition];
                newPosition++;
            }

            this.items = temp;
            this.head = 0;
            this.tail = length;
        }

    }
}
