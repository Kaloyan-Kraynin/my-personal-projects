﻿using Belote_Double_Dummy.Enums;
using Belote_Double_Dummy.Helpers;
using Belote_Double_Dummy.Models.Contracts;
using System.Text;

namespace Belote_Double_Dummy.Models
{
    public class Hand
    {
        private Dictionary<Suit, List<Card>> _cards;
        private readonly ITrumpManager _trumpManager;

        public Hand(ITrumpManager trumpManager)
        {
            _trumpManager = trumpManager;
            _cards = new();
        }

        public Hand(string handString, ITrumpManager trumpManager) : this(trumpManager)
        {
            _cards = ToCardDictionaty(handString);
        }

        public Hand(Hand other, ITrumpManager trumpManager) : this(trumpManager)
        {
            _cards = other._cards;
        }

        public void OrderCardsDescending()
        {
            for (Suit suit = Suit.Clubs; suit <= Suit.Spades; suit++)
            {
                _cards.TryGetValue(suit, out var cardList);
                if(cardList is null)
                {
                    continue;
                }
                cardList.Sort();
                cardList.Reverse();
            }
        }

        public IReadOnlyDictionary<Suit, List<Card>> Cards
        {
            get
            {
                return _cards;
            }
        }

        public IReadOnlyCollection<Card> AllCards
        {
            get
            {
                return _cards[Suit.Spades].Concat(_cards[Suit.Hearts]).Concat(_cards[Suit.Diamonds]).Concat(_cards[Suit.Clubs]).ToList();
            }
        }

        public Card? RemoveCard(Card card)
        {
            bool validS = _cards.TryGetValue(card.Suit, out var suitCards);
            bool validC = validS ? suitCards!.Remove(card) : validS;
            return validC ? card : null;
        }

        public bool AddCard(Card card)
        {
            bool validS = _cards.TryGetValue(card.Suit, out var suitCards);
            if (!validS || suitCards!.Contains(card))
            {
                return false;
            }
            var last = suitCards!.FindLast(c => c.CompareTo(card) > 0);
            if (last is null)
            {
                suitCards.Insert(0, card);
                return true;
            }
            int index = suitCards.LastIndexOf(last) + 1;
            suitCards.Insert(index, card);
            return true;
        }

        public override string ToString()
        {
            StringBuilder sb = new();
            for (Suit suit = Suit.Spades; suit >= Suit.Clubs; suit--)
            {
                sb.Append($"{suit.ToSuitChar()} ");
                _cards.TryGetValue(suit, out var cardList);
                if (cardList is null || cardList.Count == 0)
                {
                    sb.AppendLine("---");
                    continue;
                }
                foreach(var card in cardList)
                {
                    sb.Append(card.NameChar);
                }
                sb.AppendLine();
            }
            return sb.ToString().TrimEnd();
        }

        private Dictionary<Suit, List<Card>> ToCardDictionaty(string handString)
        {
            Dictionary<Suit, List<Card>> cards = new();
            string[] suits = handString.Split(" ");
            if (suits.Length != 4)
            {
                throw new ArgumentException("Invalid hand string format.");
            }
            Suit suit = Suit.Spades;
            foreach(var suitStr in suits)
            {
                cards.TryAdd(suit, SuitStringToCards(suitStr, suit).ToList());
                suit--;
            }
            return cards;
        }

        private IEnumerable<Card> SuitStringToCards(string suitString, Suit suit)
        {
            if (string.IsNullOrEmpty(suitString) || suitString.Contains('-')) yield break;
            foreach (var c in suitString)
            {
                yield return new Card(suit, c.ToCardName(), _trumpManager);
            }
        }


    }
}
