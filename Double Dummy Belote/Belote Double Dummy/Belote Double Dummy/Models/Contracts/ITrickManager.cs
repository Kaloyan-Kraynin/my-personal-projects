﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Belote_Double_Dummy.Models.Contracts
{
    internal interface ITrickManager
    {
        public void Push(Card card);

        public void PopCard();

        public Trick? Top { get; }

        public Player OnLead { get; }

        int OnLeadIndex { get; }

        public Player[] Players { get; }

        IEnumerable<Card> GetPlayable(Player player);
    }
}
