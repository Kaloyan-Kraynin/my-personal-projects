﻿using Belote_Double_Dummy.Enums;

namespace Belote_Double_Dummy.Models.Contracts
{
    public interface ITrumpManager
    {
        IReadOnlyCollection<Suit> CurrentTrumps { get; }

        void SetToNoTrump();

        void SetToAllTrumps();

        void SetSuitTrump(Suit trump);

        int GameTotalPoints { get; }
    }
}
