﻿using Belote_Double_Dummy.Enums;
using Belote_Double_Dummy.Models.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Belote_Double_Dummy.Models
{
    internal class TrickManager : ITrickManager
    {
        private readonly ITrumpManager _trumpManager;
        private readonly Stack<Trick> _tricks;
        private readonly Stack<int> _leadIndexes;
        private readonly Player[] _players;

        public TrickManager(ITrumpManager trumpManager, Player[] players)
        {
            _trumpManager = trumpManager;
            _tricks = new();
            _players = players;
            _leadIndexes = new();
            _leadIndexes.Push(0);
        }
        public Trick? Top
        {
            get
            {
                if (_tricks.TryPeek(out Trick? result))
                {
                    return result;
                }
                return null;
            }
        }

        public Player OnLead => _players[OnLeadIndex];

        public Player[] Players => _players;

        public int OnLeadIndex => _leadIndexes.Peek();

        public void PopCard()
        {
            if (Top!.IsFull)
            {
                var (player, score) = Top!.HandleWhenFull();
                player!.Score -= score;
            }
            if (_tricks.Count == 0)
            {
                return;
            }
            if (!_tricks.Peek().PopCard())
            {
                _tricks.Pop();
            }
            _leadIndexes.Pop();
        }

        public void Push(Card card)
        {
            if (!_tricks.Any() || Top!.IsFull)
            {
                _tricks.Push(new Trick(card, OnLead, _trumpManager));
            }
            else
            {
                Top!.AddCard(card, OnLead);
            }
            if (Top!.IsFull)
            {
                var (player, score) = Top!.HandleWhenFull();
                int takerIndex = Array.IndexOf(_players, player);
                _leadIndexes.Push(takerIndex);
                player!.Score += score;
                return;
            }
            _leadIndexes.Push((OnLeadIndex + 1) % 4);
        }

        public IEnumerable<Card> GetPlayable(Player player)
        {
            if (Top is null || Top.IsFull)
            {
                return player.Hand!.AllCards;
            }
            var cards = player.Hand!.Cards;
            var suit = Top.Suit;
            cards.TryGetValue(suit, out var suitCards);
            if (suitCards is null)
            {
                throw new ArgumentException("Invalid hand, suit is missing.");
            }
            if (suitCards.Count > 0)
            {
                return PlayableWhenHasSuitAsked(Top, suitCards);
            }
            Suit trumpSuit = _trumpManager.CurrentTrumps.FirstOrDefault();
            if (_trumpManager.CurrentTrumps.Count != 1 || Top.CurrentTakingPlayer == player.Partner || player.Hand.Cards[trumpSuit].Count == 0)
            {
                return player.Hand.AllCards;
            }
            var trumps = player.Hand.Cards[trumpSuit];
            Card currentTaker = Top.CurrentTakingCard;
            if (currentTaker.Suit != trumpSuit)
            {
                return trumps;
            }
            var toppers = trumps.Where(c => c.CompareTo(currentTaker) > 0);
            return !toppers.Any() ? trumps : toppers;
        }

        private IEnumerable<Card> PlayableWhenHasSuitAsked(Trick trick, List<Card> suitCards)
        {
            if (!_trumpManager.CurrentTrumps.Contains(trick.Suit))
            {
                return suitCards;
            }

            var carToTop = trick.GetLastCorrectSuitCard();
            var toppers = suitCards.Where(c => c.CompareTo(carToTop) > 0);
            if (!toppers.Any())
            {
                return suitCards;
            }
            return toppers;
        }
    }
}
