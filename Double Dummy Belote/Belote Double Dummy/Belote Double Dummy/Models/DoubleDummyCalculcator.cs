﻿using Belote_Double_Dummy.Helpers;
using Belote_Double_Dummy.Models.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Belote_Double_Dummy.Models
{
    public class DoubleDummyCalculcator
    {
        private readonly ITrumpManager _trumpManager;
        private readonly ITrickManager _trickManager;

        public DoubleDummyCalculcator(string[] handStrings, ITrumpManager trumpManager)
        {
            Player[] players = new Player[4];
            if (handStrings.Length != 4)
            {
                throw new ArgumentException("The number of hands in a deal must be 4");
            }
            _trumpManager = trumpManager;
            for (int i = 0; i < 4; i++)
            {
                var hand = new Hand(handStrings[i], _trumpManager);
                players[i] = new Player()
                {
                    Hand = hand
                };
            }
            players[0].Partner = players[2];
            players[1].Partner = players[3];
            _trickManager = new TrickManager(_trumpManager, players);
        }

        public Dictionary<Card, int> CalculateOutcome()
        {
            var outcome = new Dictionary<Card, int>();
            var leader = _trickManager.OnLead;
            var playable = _trickManager.GetPlayable(leader).ToArray();
            foreach (var card in playable)
            {
                var result = CalculateOutcome(leader, card).ForResult;
                outcome.Add(card, result);
                Console.WriteLine($"yay score:{leader.Score} tricks:{_trickManager.Top is null} leaderIndex:{_trickManager.OnLeadIndex}");
                Console.WriteLine(leader.Hand!);
                Console.WriteLine();
            }
            return outcome;
        }

        public int BestOutcome()
        {
            var leader = _trickManager.OnLead;
            return CalculateOutcome(leader, leader).ForPlayer;
        }

        private ScoreResult CalculateOutcome(Player player, Player forResult)
        {
            var playable = _trickManager.GetPlayable(player).ToArray();
            if (!playable.Any())
            {
                return new ScoreResult()
                {
                    ForPlayer = player.Score,
                    ForResult = forResult.Score
                };
            }
            List<ScoreResult> results = new();
            foreach (var card in playable)
            {
                results.Add(CalculateOutcome(player, card));
            }
            return results.Max();
        }

        private ScoreResult CalculateOutcome(Player player, Card card)
        {
            player.Hand!.RemoveCard(card);
            _trickManager.Push(card);
            ScoreResult result = CalculateOutcome(_trickManager.OnLead, player);
            _trickManager.PopCard();
            player.Hand!.AddCard(card);
            return result;
        }

        private struct ScoreResult : IComparable<ScoreResult>
        {
            public int ForResult;
            public int ForPlayer;

            public int CompareTo(ScoreResult other)
            {
                return (ForPlayer - ForResult) - (other.ForPlayer - other.ForResult);
            }
        }
    }
}
