﻿using Belote_Double_Dummy.Enums;
using Belote_Double_Dummy.Models.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Belote_Double_Dummy.Models
{
    public class TrumpManager : ITrumpManager
    {
        readonly List<Suit> _trumps = new();
        public IReadOnlyCollection<Suit> CurrentTrumps => _trumps;

        public int GameTotalPoints
        {
            get
            {
                return _trumps.Count switch
                {
                    0 => 120,
                    1 => 162,
                    4 => 258,
                    _ => 120
                };
            }
        }
        public void SetSuitTrump(Suit trump)
        {
            _trumps.Clear();
            _trumps.Add(trump);
        }

        public void SetToAllTrumps()
        {
            _trumps.Clear();
            for (Suit suit = Suit.Clubs; suit <= Suit.Spades; suit++)
            {
                _trumps.Add(suit);
            }
        }

        public void SetToNoTrump()
        {
            _trumps.Clear();
        }
    }
}
