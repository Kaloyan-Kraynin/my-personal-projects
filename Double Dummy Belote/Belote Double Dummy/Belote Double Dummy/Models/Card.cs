﻿using Belote_Double_Dummy.Enums;
using Belote_Double_Dummy.Helpers;
using Belote_Double_Dummy.Models.Contracts;

namespace Belote_Double_Dummy.Models
{
    public class Card : IComparable<Card>
    {
        private readonly ITrumpManager _trumpManager;

        private Card(ITrumpManager trumpManager)
        {
            _trumpManager = trumpManager;
        }
        public Card(Suit suit, CardName name, ITrumpManager trumpManager) : this(trumpManager)
        {
            Suit = suit;
            Name = name;
        }

        public Suit Suit { get; set; }

        public CardName Name { get; set; }

        public int CompareTo(Card? other)
        {
            if (other is null || Suit != other.Suit) return 0;
            int difference = Points - other.Points;
            if (difference == 0) difference = Name - other.Name;
            return difference;
        }

        public int Points
        {
            get
            {
                if (IsTrump)
                {
                    switch (Name)
                    {
                        case CardName.Nine:
                            return 14;
                        case CardName.Jack:
                            return 20;
                    }
                }

                return Name switch
                {
                    <= CardName.Nine => 0,
                    CardName.Ten => 10,
                    CardName.Jack => 2,
                    CardName.Queen => 3,
                    CardName.King => 4,
                    >= CardName.Ace => 11
                };
            }
        }

        public bool IsTrump
        {
            get => _trumpManager.CurrentTrumps.Contains(Suit);
        }

        public char SuitChar
        {
            get => Suit.ToSuitChar();
        }

        public char NameChar
        {
            get
            {
                return Name.ToNameChar();
            }
        }
        public override string ToString()
        {
            return $"{SuitChar}{NameChar}";
        }
    }
}
