﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Belote_Double_Dummy.Models
{
    public class Player
    {
        private int _score;
        private Player? _partner;

        public Player? Partner 
        { 
            get => _partner; 
            set
            {
                _partner = value;
                value!._partner = this;
            }
        }

        public Hand? Hand { get; init; }

        public int Score
        {
            get => _score;
            set
            {
                _score = value;
                Partner!._score = value;
            }
        }
    }
}
