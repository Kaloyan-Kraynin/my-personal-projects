﻿using Belote_Double_Dummy.Enums;
using Belote_Double_Dummy.Models.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Belote_Double_Dummy.Models
{
    public class Trick
    {
        private readonly ITrumpManager _trumpManager;
        private Card[] _cards = new Card[4];
        private Player[] _players = new Player[4];
        private int _cardCount;
        private Suit _suit;

        public Trick(Card card, Player player, ITrumpManager trumpManager)
        {
            _trumpManager = trumpManager;
            Add(card, player);
            _suit = card.Suit;
        }

        public bool AddCard(Card card, Player player)
        {
            if (IsFull)
            {
                return false;
            }
            Add(card, player);
            return true;
        }

        public bool PopCard()
        {
            if (_cardCount == 1)
            {
                return false;
            }
            _cardCount--;
            return true;
        }

        public Suit Suit { get => _suit; }

        public bool IsFull { get => _cardCount == 4; }

        public Card CurrentTakingCard
        {
            get
            {
                return _cards[CurrentTakingIndex];
            }
        }

        public Player CurrentTakingPlayer
        {
            get
            {
                return _players[CurrentTakingIndex];
            }
        }
        public (Player? taker, int score) HandleWhenFull()
        {
            if (_cardCount < 4)
            {
                return (null, 0);
            }
            int score = _cards.Sum(c => c.Points);
            Player player = CurrentTakingPlayer;
            return (player, score);
        }

        public Card GetLastCorrectSuitCard()
        {
            Card result = _cards[0];
            for (int i = 1; i < _cardCount; i++)
            {
                if (_cards[i].Suit == _suit)
                {
                    result = _cards[i];
                }
            }
            return result;
        }

        private int CurrentTakingIndex
        { 
            get
            {
                Suit trumpSuit = _trumpManager.CurrentTrumps.FirstOrDefault();
                if(_trumpManager.CurrentTrumps.Count != 1 || !_cards.Select(c => c.Suit).Contains(trumpSuit))
                {
                    return GetTakerIndexUnsuited();
                }
                var taker = _cards.Where(c => c.Suit == trumpSuit).Max();
                return Array.IndexOf(_cards, taker);
            }
        }

        private void Add(Card card, Player player)
        {
            _cards[_cardCount] = card;
            _players[_cardCount] = player;
            _cardCount++;
        }

        private int GetTakerIndexUnsuited()
        {
            int takerIndex = 0;
            for (int i = 1; i < 4; i++)
            {
                if (_cards[i].Suit != _suit)
                {
                    continue;
                }
                if (_cards[i].CompareTo(_cards[takerIndex]) > 0)
                {
                    takerIndex = i;
                }
            }

            return takerIndex;
        }
    }
}
