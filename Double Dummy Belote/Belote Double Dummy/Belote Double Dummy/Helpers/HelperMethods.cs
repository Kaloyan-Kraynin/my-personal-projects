﻿using Belote_Double_Dummy.Enums;
using Belote_Double_Dummy.Models;
using Belote_Double_Dummy.Models.Contracts;

namespace Belote_Double_Dummy.Helpers
{
    public static class HelperMethods
    {
        public static CardName ToCardName(this char cardChar)
        {
            return char.ToUpper(cardChar) switch
            {
                '7' => CardName.Seven,
                '8' => CardName.Eight,
                '9' => CardName.Nine,
                'T' => CardName.Ten,
                'J' => CardName.Jack,
                'Q' => CardName.Queen,
                'K' => CardName.King,
                'A' => CardName.Ace,
                _ => throw new ArgumentException("Invalid cardChar.")
            };
        }

        public static char ToSuitChar(this Suit suit)
        {
            return suit.ToString()[0];
        }

        public static char ToNameChar(this CardName name)
        {
            return name <= CardName.Nine ? (char)('0' + ((int)name)) : name.ToString()[0];
        }

        public static IEnumerable<Card> GetPlayable(Player player, ITrumpManager trumpManager, Trick? trick)
        {
            if (trick is null || 
                trick.IsFull)
            {
                return player.Hand!.AllCards;
            }
            var cards = player.Hand!.Cards;
            var suit = trick.Suit;
            cards.TryGetValue(suit, out var suitCards);
            if (suitCards is null)
            {
                throw new ArgumentException("Invalid hand, suit is missing.");
            }
            if(suitCards.Count > 0)
            {
                return PlayableWhenHasSuitAsked(trumpManager, trick, suitCards);
            }
            Suit trumpSuit = trumpManager.CurrentTrumps.FirstOrDefault();
            if(trumpManager.CurrentTrumps.Count != 1 || trick.CurrentTakingPlayer == player.Partner || player.Hand.Cards[trumpSuit].Count == 0)
            {
                return player.Hand.AllCards;
            }
            var trumps = player.Hand.Cards[trumpSuit];
            Card currentTaker = trick.CurrentTakingCard;
            if (currentTaker.Suit != trumpSuit)
            {
                return trumps;
            }
            var toppers = trumps.Where(c => c.CompareTo(currentTaker) > 0);
            return !toppers.Any() ? trumps : toppers;
        }

        private static IEnumerable<Card> PlayableWhenHasSuitAsked(ITrumpManager trumpManager, Trick trick, List<Card> suitCards)
        {
            if (!trumpManager.CurrentTrumps.Contains(trick.Suit))
            {
                return suitCards;
            }

            var carToTop = trick.GetLastCorrectSuitCard();
            var toppers = suitCards.Where(c => c.CompareTo(carToTop) > 0);
            if (!toppers.Any())
            {
                return suitCards;
            }
            return toppers;
        }
    }
}
