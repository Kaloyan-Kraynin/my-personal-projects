﻿using Belote_Double_Dummy.Enums;
using Belote_Double_Dummy.Models;
using Belote_Double_Dummy.Models.Contracts;
using System.Text;

ITrumpManager trumpManager = new TrumpManager();
string[] input = new string[4];
for (int i = 0; i < 4; i++)
{
    input[i] = Console.ReadLine()!;
}
DoubleDummyCalculcator ddc = new(input, trumpManager);
Dictionary<Card, int> result = ddc.CalculateOutcome();
foreach (var item in result)
{
    Console.WriteLine($"{item.Key} -> {item.Value}");
}
/*Console.WriteLine(ddc.BestOutcome());*/
