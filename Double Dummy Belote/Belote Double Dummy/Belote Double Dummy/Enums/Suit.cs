﻿namespace Belote_Double_Dummy.Enums
{
    public enum Suit
    {
        Clubs,
        Diamonds,
        Hearts,
        Spades
    }
}
